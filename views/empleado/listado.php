<?php

use yii\helpers\Html;
use yii\widgets\ListView;

echo Html::a('Tabla', [$vista, "id" => 1], ["class" => "btn btn-primary"]);
echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_listado",
    "itemOptions" => [
        'class' => 'col-3',
        'style' => [
            'border' => '5px cornflowerblue solid',
            'border-radius' => '10px',
            'margin' => '10px']
    ],
    "options" => [
        'class' => 'row justify-content-center',
    ],
    'layout' => "{items}"
]);

<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo Html::a('Tarjetas', [$vista, "id" => 2], ["class" => "btn btn-primary"]);

echo GridView::widget([
    "dataProvider" => $dataProvider
]);

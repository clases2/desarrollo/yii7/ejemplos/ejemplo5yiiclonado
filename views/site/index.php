<?php
/** @var yii\web\View $this */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas</h1>

        <p class="lead">Realizando consultas de seleccion</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col">
                <h2>Consulta 1</h2>

                <p>Listar los empleados cuyo departamento es 1.</p>

                <?=
                \yii\helpers\Html::a("Ejecutar", ["empleado/consulta1", "id" => 1], [
                    "class" => "btn btn-primary"
                ])
                ?>
            </div>
            <div class="col">
                <h2>Consulta 2</h2>

                <p>Listar los empleados cuyo departamento es el 1 o el 2.</p>

                <?=
                \yii\helpers\Html::a("Ejecutar", ["empleado/consulta2", "id" => 1], [
                    "class" => "btn btn-primary"
                ])
                ?>
            </div>
            <div class="col">
                <h2>Consulta 3</h2>

                <p>Listar los empleados cuyo departamento es desarrollo.</p>
                <?=
                \yii\helpers\Html::a("Ejecutar", ["empleado/consulta3", "id" => 1], [
                    "class" => "btn btn-primary"
                ])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h2>Consulta 4</h2>

                <p>Listar todos los empleados con el nombre del departamento para el que trabajan.</p>
                <?=
                \yii\helpers\Html::a("Ejecutar", ["empleado/consulta4"], [
                    "class" => "btn btn-primary"
                ])
                ?>
            </div>
            <div class="col">
                <h2>Consulta 5</h2>

                <p>Listar el nif, el nombre y los apellidos de los empleados que trabajan en un departamento cuyos gastos exceden los 30.000 euros. Ademas quiero el nombre de los departamentos y los gastos.</p>
                <?=
                \yii\helpers\Html::a("Ejecutar", ["empleado/consulta5"], [
                    "class" => "btn btn-primary"
                ])
                ?>
            </div>
            <div class="col">
                <h2>Consulta 6</h2>

                <p>Listar los empleados (todos los campos) que trabajan en un departamento cuyo presupuesto esta entre 100.000 y 150.000 euros.</p>
                <?=
                \yii\helpers\Html::a("Ejecutar", ["empleado/consulta6"], [
                    "class" => "btn btn-primary"
                ])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h2>Consulta 7</h2>

                <p>Listar los empleados (todos los campos) que trabajan en un departamento cuyo presupuesto esta entre 100.000 y 150.000 euros y cuyo nombre (del empleado) comience por A.</p>
                <?=
                \yii\helpers\Html::a("Ejecutar", ["empleado/consulta7"], [
                    "class" => "btn btn-primary"
                ])
                ?>
            </div>
        </div>

    </div>
</div>

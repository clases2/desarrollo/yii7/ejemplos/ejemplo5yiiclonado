<?php

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = "Lista de empleados en el departamento $nombre.";
$this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo GridView::widget([
    "dataProvider" => $dataProvider
]);

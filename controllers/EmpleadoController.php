<?php

namespace app\controllers;

use app\models\Departamento;
use app\models\Empleado;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpleadoController implements the CRUD actions for Empleado model.
 */
class EmpleadoController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Empleado models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Empleado::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'codigo' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empleado model.
     * @param int $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo) {
        return $this->render('view', [
                    'model' => $this->findModel($codigo),
        ]);
    }

    /**
     * Creates a new Empleado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function actionCreate() {
        $model = new Empleado();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo' => $model->codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Empleado model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo Codigo
     * @return string|Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo) {
        $model = $this->findModel($codigo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo' => $model->codigo]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Empleado model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo Codigo
     * @return Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo) {
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Empleado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo Codigo
     * @return Empleado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo) {
        if (($model = Empleado::findOne(['codigo' => $codigo])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConsulta1($id) {
        // creo el activeQuery
        $consulta = Empleado::find()
                ->where([
            "codigo_departamento" => 1
        ]);
        // crear el dataProvider
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);

        if ($id == 1) {
            $vista = "consultas"; // Muestra lso datos en un gridView.
        } else {
            $vista = "listado"; // Muestra los datos en una listaView.
        }

        // el dataProvider lo mandamos a la vista
        return $this->render($vista, [
                    "dataProvider" => $dataProvider,
            "vista" => "empleado/consulta1"
        ]);
    }

    public function actionConsulta2($id) {
        // creo el activeQuery
        // parametrizando la consulta
        $consulta = Empleado::find()
                ->where([
            "OR",
            ["codigo_departamento" => 1],
            ["codigo_departamento" => 2],
        ]);
        // utilizando la consulta como texto
        // $consulta = Empleado::find()
        //        ->where("codigo_departamento=1 OR codigo_departamento=2");
        // crear el dataProvider
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        if ($id == 1) {
            $vista = "consultas"; // Muestra lso datos en un gridView.
        } else {
            $vista = "listado"; // Muestra los datos en una listaView.
        }
        // el dataProvider lo mandamos a la vista
        return $this->render($vista, [
                    "dataProvider" => $dataProvider,
                    "vista" => "empleado/consulta2"
        ]);
    }

    public function actionConsulta3($id) {
        // Sacando el codigo del departamento desarrollo.
        $c = Departamento::find()
                        ->where([
                            "nombre" => "Desarrollo"
                        ])->one()->codigo;

        // creo el activeQuery
        // parametrizando la consulta
        $consulta = Empleado::find()
                ->where([
            "codigo_departamento" => $c,
        ]);
        // Hacerlo todo en una sola consulta.
        // $consulta = Empleado::find()->joinWith("codigoDepartamento d", true, "inner join")
        //        ->where(["d.nombre" => "desarrollo"]);
        // crear el dataProvider
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        if ($id == 1) {
            $vista = "consultas"; // Muestra lso datos en un gridView.
        } else {
            $vista = "listado"; // Muestra los datos en una listaView.
        }
        // el dataProvider lo mandamos a la vista
        return $this->render($vista, [
                    "dataProvider" => $dataProvider,
                    "vista" => "empleado/consulta3"
        ]);
    }

    public function actionConsulta4() {
        // creo el activeQuery
        $consulta = Empleado::find();
        // crear el dataProvider
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta,
            "pagination" => [
                "pageSize" => 3
            ]
        ]);
        // el dataProvider lo mandamos a la vista
        return $this->render("consultas1", [
                    "dataProvider" => $dataProvider,
                    "campos" => [
                        "codigo",
                        "nombre",
                        "nif",
                        "apellido1",
                        "apellido2",
                        "codigoDepartamento.nombre"
                    ]
        ]);
    }

    public function actionConsulta5() {
        // Opcion 1 usando directamente el joinwith. Creo el activeQuery.
        $consulta = Empleado::find()
                ->joinWith("codigoDepartamento", true, "inner join")
                ->where("gastos>30000");
        // Creo el dataProvider.
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        // El dataProvider lo mandamos a la vista.
        return $this->render("consultas1", [
                    "dataProvider" => $dataProvider,
                    "campos" => [
                        "nif",
                        "nombre",
                        "apellido1",
                        "apellido2",
                        "codigoDepartamento.nombre",
                        "codigoDepartamento.gastos"
                    ]
        ]);
        // Fin de la opcion 1.
    }

    public function actionConsulta6() {
        // Creo el activeQuery.
        $consulta = Empleado::find()
                ->joinWith("codigoDepartamento", true, "inner join")
                ->where("presupuesto between 100000 and 150000");
        // Creo el dataProvider.
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        // El dataProvider lo mandamos a la vista.
        return $this->render("consultas1", [
                    "dataProvider" => $dataProvider,
                    "campos" => [
                        "nif",
                        "codigo",
                        "nombre",
                        "apellido1",
                        "apellido2",
                        "codigoDepartamento.nombre",
                        "codigoDepartamento.presupuesto"
                    ]
        ]);
    }

    public function actionConsulta7() {
        // Creo el activeQuery.
        $consulta = Empleado::find()
                ->joinWith("codigoDepartamento", true, "inner join")
                ->where("presupuesto between 100000 and 150000 and empleado.nombre like 'A%'");
        // Creo el dataProvider.
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        // El dataProvider lo mandamos a la vista.
        return $this->render("consultas1", [
                    "dataProvider" => $dataProvider,
                    "campos" => [
                        "nif",
                        "codigo",
                        "nombre",
                        "apellido1",
                        "apellido2",
                        "codigoDepartamento.nombre",
                        "codigoDepartamento.presupuesto"
                    ]
        ]);
    }

}

<?php

namespace app\controllers;

use app\models\Empleado;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * 
     * accion preparada para entender los conceptos de
     * consultas desde Yii
     * ActiveQuery
     * ActiveRecord
     * 
     */
    public function actionEmpleados() {

        // active query
        // clase de yii para consultas sql

        $aq = Empleado::find();  // select * from empleado;
        // array de activeRecords
        // Al ejecutar la consulta tengo una array
        // de registros activos
        $vectorAR = $aq->all(); // consulta ejecutada
        // leer
        //echo $vectorAR[1]->nombre; // leo el nombre del segundo empleado
        // escribir
        //$vectorAR[1]->nombre="jose"; 
        //$vectorAR[1]->save();
        // array de arrays

        $vectorArrays = $aq->asArray()->all();

        // leer
        // echo $vectorArrays[1]['nombre'];
        // consulta1
        // buscar el empleado cuyo codigo es 1
        //$consulta1=Empleado::findAll([
        //    'codigo' => 1
        // ]);
        // $consulta1=Empleado::findAll([1]);
        // $consulta1=Empleado::find()
        //         ->where([
        //            'codigo' => 1
        //        ])
        //        ->all();

        $consulta1 = Empleado::find()
                ->where("codigo=1")
                ->all();

        // quiero mostrar los empleados cuyos codigos
        // estan entre 1 y 5

        /* $consulta2=Empleado::find()
          ->where("codigo between 1 and 5")
          ->all();

          $consulta2=Empleado::find()
          ->where("codigo>=1 and codigo<=5")
          ->all(); */

        $consulta2 = Empleado::find()
                ->where([
                    "and",
                    [">=", "codigo", 1], //Primero se coloca el operador.
                    ["<=", "codigo", 5] //Quedarian los codigos mayor o igual a 1 y menor o igual a 5.
                ])
                ->all();

        $consulta2 = Empleado::find()
                ->where([
                    "and",
                    ["codigo" => 1],
                    ["codigo" => 5]
                ])
                ->all();

        $consulta1 = Empleado::find()
                ->where([
            "codigo" => 1
        ]); // activeQuery

        $consulta2 = Empleado::find()
                ->where([
            "and",
            [">=", "codigo", 1], //Primero se coloca el operador.
            ["<=", "codigo", 5] //Quedarian los codigos mayor o igual a 1 y menor o igual a 5.
        ]); //activeQuery
        // listar los empleados cuyo departamento 
        // es 1

        $consulta3 = Empleado::find()
                ->where([
            "codigo_departamento" => 1
        ]);

        $dataProvider = new ActiveDataProvider([
            "query" => $consulta2
        ]);

        return $this->render('empleados', [
                    "registros" => $dataProvider
        ]);
    }

    

}

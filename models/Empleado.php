<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleado".
 *
 * @property int $codigo
 * @property string $nif
 * @property string $nombre
 * @property string $apellido1
 * @property string|null $apellido2
 * @property int|null $codigo_departamento
 *
 * @property Departamento $codigoDepartamento
 */
class Empleado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nif', 'nombre', 'apellido1'], 'required'],
            [['codigo_departamento'], 'integer'],
            [['nif'], 'string', 'max' => 9],
            [['nombre', 'apellido1', 'apellido2'], 'string', 'max' => 100],
            [['nif'], 'unique'],
            [['codigo_departamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamento::className(), 'targetAttribute' => ['codigo_departamento' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nif' => 'Nif',
            'nombre' => 'Nombre',
            'apellido1' => 'Apellido 1',
            'apellido2' => 'Apellido 2',
            'codigo_departamento' => 'Codigo Departamento',
        ];
    }

    /**
     * Gets query for [[CodigoDepartamento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDepartamento()
    {
        return $this->hasOne(Departamento::className(), ['codigo' => 'codigo_departamento']);
    }
}
